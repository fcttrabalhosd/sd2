package microgram.impl.dropbox;

import microgram.api.java.Media;
import microgram.api.java.Result;

import static microgram.api.java.Result.error;
import static microgram.api.java.Result.ErrorCode.CONFLICT;
import static microgram.api.java.Result.ErrorCode.INTERNAL_ERROR;
import static microgram.api.java.Result.ErrorCode.NOT_FOUND;

import java.io.File;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.apache.zookeeper.common.IOUtils;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.pac4j.scribe.builder.api.DropboxApi20;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.sun.net.httpserver.HttpServer;

import microgram.impl.dropbox.msgs.*;
import microgram.api.java.Result;
import utils.Hash;
import utils.JSON;

public class DropboxMedia implements Media {

	private static final String apiKey = "a28066pqp5x5s8j";
	private static final String apiSecret = "hbs8um3vjhno5xj";
	private static final String accessTokenStr = "BIorW2TMvUAAAAAAAAABQQy2I6CyQwtlVEEkv4QQQBwtTWYdaoX75ru2aO04S-vr";

	protected static final String JSON_CONTENT_TYPE = "application/json; charset=utf-8";
	protected static final String OCTETSTREAM_CONTENT_TYPE = "application/octet-stream";

	private static final String CREATE_FILE_V2_URL = "https://content.dropboxapi.com/2/files/upload";
	private static final String DELETE_FILE_V2_URL = "https://api.dropboxapi.com/2/files/delete_v2";
	private static final String DOWNLOAD_FILE_V2_URL = "https://content.dropboxapi.com/2/files/download";

	private static final String DROPBOX_API_ARG = "Dropbox-API-Arg";

	protected OAuth20Service service;
	protected OAuth2AccessToken accessToken;
	
	/**
	 * Creates a dropbox client, given the access token.
	 * @param accessTokenStr String with the previously obtained access token.
	 * @throws Exception Throws exception if something failed.
	 */
	public static DropboxMedia createClientWithAccessToken() throws Exception {
		try {
			OAuth20Service service = new ServiceBuilder(apiKey).apiSecret(apiSecret).build(DropboxApi20.INSTANCE);
			OAuth2AccessToken accessToken = new OAuth2AccessToken(accessTokenStr);

			System.err.println(accessToken.getAccessToken());
			System.err.println(accessToken.toString());
			return new DropboxMedia( service, accessToken); 

		} catch (Exception x) {
			x.printStackTrace();
			throw new Exception(x);
		}
	}

	/**
	 * Creates a dropbox client, given the access token.
	 * @param accessTokenStr String with the previously obtained access token.
	 * @throws Exception Throws exception if something failed.
	 */
	public static DropboxMedia createClientWithAccessToken(String accessTokenStr) throws Exception {
		try {
			OAuth20Service service = new ServiceBuilder(apiKey).apiSecret(apiSecret).build(DropboxApi20.INSTANCE);
			OAuth2AccessToken accessToken = new OAuth2AccessToken(accessTokenStr);

			System.err.println(accessToken.getAccessToken());
			System.err.println(accessToken.toString());
			return new DropboxMedia( service, accessToken); 

		} catch (Exception x) {
			x.printStackTrace();
			throw new Exception(x);
		}
	}

	/**
	 * Creates a dropbox client, given a file containing an access token.
	 * @param accessTokenFile File containing the previously obtained access token.
	 * @throws Exception Throws exception if something failed.
	 */
	public static DropboxMedia createClientWithAccessTokenFile(File accessTokenFile) throws Exception {
		try {
			String accessTokenStr = new String(Files.readAllBytes(accessTokenFile.toPath()), StandardCharsets.UTF_8);
			return createClientWithAccessToken(accessTokenStr);
		} catch (Exception x) {
			x.printStackTrace();
			throw new Exception(x);
		}
	}

	protected DropboxMedia(OAuth20Service service, OAuth2AccessToken accessToken) {
		this.service = service;
		this.accessToken = accessToken;
	}
	
	@Override
	public Result<String> upload(byte[] bytes) {
		try {
			OAuthRequest uploadFile = new OAuthRequest(Verb.POST, CREATE_FILE_V2_URL);
			uploadFile.addHeader("Content-Type", OCTETSTREAM_CONTENT_TYPE);
			
			String filename = Hash.of(bytes);
			uploadFile.addHeader("Dropbox-API-Arg",JSON.encode(new CreateFileV2Args("/xpto/"+filename)));
			uploadFile.setPayload(bytes);
			
			System.out.println(uploadFile.toString());
	
			service.signRequest(accessToken, uploadFile);
			Response r = service.execute(uploadFile);
	
			if (r.getCode() == 409) {
				System.err.println("Dropbox File already exists");
				return Result.error(Result.ErrorCode.CONFLICT);
			} else if (r.getCode() == 200) {
				System.err.println("Dropbox File was created with success");
				return Result.ok(filename);
			} else {
				System.err.println("Unexpected error HTTP: " + r.getCode());
				return Result.error(Result.ErrorCode.INTERNAL_ERROR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error(Result.ErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	public Result<byte[]> download(String id) {
		try {
			OAuthRequest downloadFile = new OAuthRequest(Verb.GET, DOWNLOAD_FILE_V2_URL);
			
			downloadFile.addHeader("Dropbox-API-Arg", JSON.encode(new AccessFileV2Args("/xpto/"+id)));
			System.out.println(downloadFile.toString());
			System.out.println(downloadFile.getHeaders());
			service.signRequest(accessToken, downloadFile);
			Response r = service.execute(downloadFile);
			System.out.println(r.getMessage());
			if (r.getCode() == 200) {
				System.err.println("Success");
				return Result.ok(r.getBody().getBytes());
			} else {
				System.err.println("Unexpected error HTTP: " + r.getCode());
				return Result.error(Result.ErrorCode.INTERNAL_ERROR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error(Result.ErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	public Result<Void> delete(String id) {
		try {
			OAuthRequest deleteFile = new OAuthRequest(Verb.POST, DELETE_FILE_V2_URL);
			deleteFile.addHeader("Content-Type", JSON_CONTENT_TYPE);
			
			deleteFile.setPayload(JSON.encode(new AccessFileV2Args("/xpto/"+id)));
			
			System.out.println(deleteFile.toString());
			System.out.println(deleteFile.getHeaders());
			service.signRequest(accessToken, deleteFile);
			Response r = service.execute(deleteFile);
			
			System.err.println("\nCode "+r.getCode());
			
			if (r.getCode() == 404) {
				System.err.println("Dropbox File no exists");
				return Result.error(Result.ErrorCode.NOT_FOUND);
			} else if (r.getCode() == 200) {
				System.err.println("Success");
				return Result.ok();
			} else {
				System.err.println("Unexpected error HTTP: " + r.getCode());
				return Result.error(Result.ErrorCode.INTERNAL_ERROR);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error(Result.ErrorCode.INTERNAL_ERROR);
		}
	}

}
