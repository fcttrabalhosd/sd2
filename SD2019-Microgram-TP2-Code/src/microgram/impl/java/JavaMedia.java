package microgram.impl.java;

import microgram.api.java.Media;
import microgram.api.java.Result;
import microgram.impl.dropbox.DropboxMedia;

public class JavaMedia implements Media {
	
	public DropboxMedia dropboxMedia;

	public JavaMedia() {		
		try {
			dropboxMedia = DropboxMedia.createClientWithAccessToken();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Result<String> upload(byte[] bytes) {
		try {
			Result<String> res = dropboxMedia.upload(bytes);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error(Result.ErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	public Result<byte[]> download(String id) {
		try {
			Result<byte[]> res = dropboxMedia.download(id);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error(Result.ErrorCode.INTERNAL_ERROR);
		}
	}

	@Override
	public Result<Void> delete(String id) {
		try {
			Result<Void> res = dropboxMedia.delete(id);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return Result.error(Result.ErrorCode.INTERNAL_ERROR);
		}
	}
}
