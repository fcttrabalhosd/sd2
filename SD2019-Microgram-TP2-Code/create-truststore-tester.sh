cp base-truststore.ks tester-truststore.ks
echo "Use password: changeit"
keytool -importcert -file mediaserver.cert -alias mediastore -keystore tester-truststore.ks
keytool -importcert -file microgram.cert -alias microgram -keystore tester-truststore.ks